var obj = {
    name: 'John Doe',
    greet: function (){
        console.log(`Hello ${ this.name}`);
    }
}
obj.greet();
/*La unica diferencia entre call y apply es que si hay un parametro
call lo puede pasar directamente con comas y apply necesita
crear un arreglo de parametros*/
obj.greet.call({name: 'Jane Doe'});
obj.greet.apply({name: 'Jane Doe'});